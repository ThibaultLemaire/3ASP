﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace _147SupinfyWebRole.Models
{
    public class HomeViewModel
    {
        public IEnumerable<Music> musics {get; set;}
                
    }

    public class SearchViewModel
    {
        public IEnumerable<Music> musics { get; set; }
        public IEnumerable<Playlist> playlists { get; set; }
        public IEnumerable<ApplicationUser> users { get; set; }
    }
}
