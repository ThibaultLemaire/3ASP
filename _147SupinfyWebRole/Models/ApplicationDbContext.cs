﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Data.Entity;
using System.Threading.Tasks;

namespace _147SupinfyWebRole.Models
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public DbSet<Music> Musics { get; set; }
        public DbSet<Playlist> Playlists { get; set; }
        public DbSet<ViewsStat> Stats { get; set; }

        public async Task<Music> FindMusicAsync(int id)
        {
            return await Musics.FindAsync(BitConverter.GetBytes(id));
        }

        public async Task<Music> CreateMusic(Music music)
        {
            Music existingEntry = await Musics.FindAsync(music.Hash);

            if (existingEntry == null)
            {
                return Musics.Add(music);
            }
            else
            {
                return existingEntry;
            }
        }

        public Music DeleteMusic(Music music)
        {
            return Musics.Remove(music);
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
    }

    public class ViewsStat
    {
        public ViewsStat()
        {

        }

        public int Id { get; set; }

        public virtual Music Music { get; set; }
    }
}