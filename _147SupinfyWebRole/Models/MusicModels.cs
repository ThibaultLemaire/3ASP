﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace _147SupinfyWebRole.Models
{
    public class Music
    {
        private string _url;

        public Music()
        {
            Playlists = new HashSet<Playlist>();
        }
        
        [Key]
        public byte[] Hash
        {
            get
            {
                return BitConverter.GetBytes(Id);
            }
            set
            {
                Id = BitConverter.ToInt32(value, 0);
            }
        }

        [NotMapped]
        public int Id { get; set; }
        
        public string Url
        {
            get
            {
                return _url;
            }
            set
            {
                _url = value;
                Id = _url.GetHashCode();
            }
        }

        public string Name { get; set; }
        public string Artist { get; set; }
        public int ViewCount { get; set; }
        public virtual ICollection<Playlist> Playlists { get; set; }
    }

    public class Playlist
    {
        public Playlist()
        {
            Musics = new HashSet<Music>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public virtual ICollection<Music> Musics { get; set; }
        public virtual ApplicationUser Owner { get; set; }
    }
}