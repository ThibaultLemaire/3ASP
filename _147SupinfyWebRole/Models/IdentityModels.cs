﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Security.Claims;
using System.Threading.Tasks;

namespace _147SupinfyWebRole.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public ApplicationUser() : base()
        {
            Playlists = new HashSet<Playlist>();
        }

        public override string Email
        {
            get
            {
                return UserName;
            }

            set
            {
                UserName = value;
            }
        }
        
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime CreationDate { get; set; }
        public UserRole Role { get; set; }
        public virtual ICollection<Playlist> Playlists { get; set; }

        [InverseProperty("Following")]
        public virtual ICollection<ApplicationUser> FollowedBy { get; set; }
        [InverseProperty("FollowedBy")]
        public virtual ICollection<ApplicationUser> Following { get; set; }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }

    public enum UserRole
    {
        Standard,
        Administrator,
    }
}