﻿using _147SupinfyWebRole.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace _147SupinfyWebRole.Controllers
{
    [Authorize]
    public class PlaylistController : Controller
    {
        ApplicationDbContext db = new ApplicationDbContext();

        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var playlist = await db.Playlists.FindAsync(id);
            return View(playlist);
        }

        public ActionResult Create()
        {
            return View(new Playlist());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Name")] Playlist playlistCurrent)
        {
            if (ModelState.IsValid)
            {
                var user = db.Users.FirstOrDefault(x => x.Email == User.Identity.Name);
                playlistCurrent.Owner = user;
                db.Playlists.Add(playlistCurrent);
                await db.SaveChangesAsync();
                return RedirectToAction("Details", new { id = playlistCurrent.Id });
            }
            return View(playlistCurrent);

        }

        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Playlist playlist = await db.Playlists.FindAsync(id);
            if (playlist == null)
            {
                return HttpNotFound();
            }
            return View(playlist);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int? id)
        {
            Playlist playlist = await db.Playlists.FindAsync(id);
            var ownerId = playlist.Owner.Id;
            db.Playlists.Remove(playlist);
            await db.SaveChangesAsync();
            return RedirectToAction("Show", "User", new { id = ownerId });
        }
    }
}