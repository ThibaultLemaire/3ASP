﻿using _147SupinfyWebRole.Models;
using System.Linq;
using System.Web.Mvc;

namespace _147SupinfyWebRole.Controllers
{
    public class HomeController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        public ActionResult Index()
        {            
            HomeViewModel model = new HomeViewModel();
            model.musics = db.Stats.Select(x => x.Music).OrderByDescending(x => x.ViewCount);
            return View(model);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";
            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";
            return View();
        }
        
        public ActionResult Search(string id)
        {
            SearchViewModel model = new SearchViewModel();
            model.musics = db.Musics.Where(x => x.Name.Contains(id) || x.Artist.Contains(id) || x.Url.Contains(id));
            model.users = db.Users.Where(x => x.FirstName.Contains(id) || x.LastName.Contains(id) || x.Id.Contains(id));
            return View(model);
        }
    }
}
