﻿using _147SupinfyWebRole.Models;
using Microsoft.AspNet.Identity.Owin;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace _147SupinfyWebRole.Controllers
{
    public class UserController : Controller
    {
        ApplicationUserManager _userManager;

        public UserController()
        {

        }

        public UserController(ApplicationUserManager userManager)
        {
            UserManager = userManager;
        }

        // GET: User
        public async Task<ActionResult> Show(string id)
        {
            var user = await UserManager.FindByIdAsync(id);
            return View(user);
        }

        [Authorize]
        public async Task<ActionResult> Follow(string id)
        {
            string username = User.Identity.Name;
            ApplicationUser user = await UserManager.FindByNameAsync(username);
            var userToFollow = await UserManager.FindByIdAsync(id);
            user.Following.Add(userToFollow);
            await UserManager.UpdateAsync(user);
            return RedirectToAction("Show", new { id = id });
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
    }
}