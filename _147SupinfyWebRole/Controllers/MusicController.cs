﻿using _147SupinfyWebRole.Models;
using System.Data.Entity;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace _147SupinfyWebRole.Controllers
{
    public class MusicController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Music
        public async Task<ActionResult> Index()
        {
            return View(await db.Musics.ToListAsync());
        }

        // GET: Music/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Music music = await db.FindMusicAsync(id.Value);
            music.ViewCount ++;
            db.Entry(music).State = EntityState.Modified;
            await db.SaveChangesAsync();
            if (music == null)
            {
                return HttpNotFound();
            }
            return View(music);
        }

        // GET: Music/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Music/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(int id,[Bind(Include = "Url,Name,Artist")] Music music)
        {
            if (ModelState.IsValid)
            {
                var returnedMusic = await db.CreateMusic(music);
                var returnedPlaylist = await db.Playlists.FindAsync(id);
                returnedPlaylist.Musics.Add(returnedMusic);

                await db.SaveChangesAsync();
                return RedirectToAction("Details", "Playlist", new { id = returnedPlaylist.Id });
            }

            return View(music);
        }

        // GET: Music/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Music music = await db.FindMusicAsync(id.Value);
            if (music == null)
            {
                return HttpNotFound();
            }
            return View(music);
        }

        // POST: Music/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Url,Name,Artist")] Music music)
        {
            if (ModelState.IsValid)
            {
                db.Entry(music).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(music);
        }

        // GET: Music/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Music music = await db.FindMusicAsync(id.Value);
            if (music == null)
            {
                return HttpNotFound();
            }
            return View(music);
        }

        // POST: Music/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int? id)
        {
            Music music = await db.FindMusicAsync(id.Value);
            db.DeleteMusic(music);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
