﻿using _147SupinfyWebRole.Models;
using Hangfire;
using Microsoft.Owin;
using Owin;
using System.Linq;

[assembly: OwinStartup(typeof(_147SupinfyWebRole.Startup))]
namespace _147SupinfyWebRole
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);

            using (ApplicationDbContext ctx = new ApplicationDbContext())
            {
                ctx.Database.Initialize(false);
            }

            GlobalConfiguration.Configuration.UseSqlServerStorage("DefaultConnection");
            app.UseHangfireDashboard();
            app.UseHangfireServer();

            RecurringJob.AddOrUpdate(() => StoreTopTen(),
                Cron.Minutely());
        }

        public static void StoreTopTen()
        {
            using (ApplicationDbContext ctx = new ApplicationDbContext())
            {
                ctx.Stats.RemoveRange(ctx.Stats);
                var topTen = ctx.Musics.OrderByDescending(x => x.ViewCount).Take(10).ToList().Select(x => new ViewsStat() { Music = x });
                ctx.Stats.AddRange(topTen);
                ctx.SaveChanges();
            }
        }
    }
}
